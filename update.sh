#!/bin/bash

echo "Would you like to update the system vimrc file? (Y/n)"
read input

if [[ $input == "n" ]] || [[ $input == "N" ]]; then
	exit 0
elif [[ $input == "y" ]] || [[ $input == "Y" ]]; then
	git clone https://gitlab.com/risa_boi/chasevim-configuration.git $HOME/Chasevim
	rm -rf $HOME/.vimrc
	mv $HOME/Chasevim/vimrc $HOME/.vimrc

	vim -c ":PlugInstall" -c ":qa"
	rm -rf $HOME/Chasevim
	exit 0
else
	echo "Doing nothing."
	exit 0
fi
