# Chasevim
Vim for ME

run this program in order to install my vim configuration:

```
bash <(curl -s https://gitlab.com/risa_boi/chasevim-configuration/raw/master/install.sh)
```

Run this program in order to update the system:
```
bash <(curl -s https://gitlab.com/risa_boi/chasevim-configuration/raw/master/update.sh)
```
